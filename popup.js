document.addEventListener('DOMContentLoaded', () => {
    const dialogBox = document.getElementById('dialog-box');
    const query = { active: true, currentWindow: true };
    chrome.tabs.query(query, (tabs) => {
        dialogBox.innerHTML = tabs[0].title;
    });
});
